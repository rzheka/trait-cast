//! Data structures in support of the `trait-cast` implementation.
//! These are not to be used directly by the crate's client.

use std::{rc::Rc, sync::Arc};

pub struct OpaqueTrait<'a>(u128, std::marker::PhantomData<&'a *mut ()>);

impl<'a> OpaqueTrait<'a> {
    #[inline]
    pub fn wrap<T: ?Sized>(t: &'a T) -> OpaqueTrait<'a> {
        if std::mem::size_of::<&T>() != std::mem::size_of::<u128>() {
            Self::panic_on_size_mismatch::<T>();
        }
        let value = unsafe { std::ptr::read(&t as *const _ as *const u128) };
        OpaqueTrait(value, Default::default())
    }

    #[inline]
    pub unsafe fn unwrap<T: ?Sized>(self) -> &'a T {
        std::ptr::read(&self.0 as *const _ as *const &T)
    }

    #[cold]
    fn panic_on_size_mismatch<T: ?Sized>() {
        panic!(
            "OpaqueTrait::wrap(&{}) unsupported type (ref size = {}), expected `&dyn Trait`",
            std::any::type_name::<T>(),
            std::mem::size_of::<&T>()
        );
    }
}

impl<'a> std::fmt::Debug for OpaqueTrait<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("OpaqueTrait(")?;
        write!(f, "0x{:x}, 0x{:x}", self.0 as u64, (self.0 >> 64) as u64)?;
        f.write_str(")")?;
        Ok(())
    }
}

pub struct OpaqueTraitArc(u128, std::marker::PhantomData<*mut ()>);

impl OpaqueTraitArc {
    #[inline]
    pub fn wrap<T: ?Sized>(t: Arc<T>) -> OpaqueTraitArc {
        if std::mem::size_of::<*const T>() != std::mem::size_of::<u128>() {
            Self::panic_on_size_mismatch::<T>();
        }
        let t = Arc::into_raw(t);
        let value = unsafe { std::ptr::read(&t as *const _ as *const u128) };
        OpaqueTraitArc(value, Default::default())
    }

    #[inline]
    pub unsafe fn unwrap<T: ?Sized>(self) -> Arc<T> {
        let ptr = std::ptr::read(&self.0 as *const _ as *const *const T);
        Arc::from_raw(ptr)
    }

    #[cold]
    fn panic_on_size_mismatch<T: ?Sized>() {
        panic!(
            "OpaqueTraitArc::wrap(Arc<{}>) unsupported type (size = {}), expected Arc<dyn Trait>",
            std::any::type_name::<T>(),
            std::mem::size_of::<*const T>()
        );
    }
}

impl<'a> std::fmt::Debug for OpaqueTraitArc {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("OpaqueTraitArc(")?;
        write!(f, "0x{:x}, 0x{:x}", self.0 as u64, (self.0 >> 64) as u64)?;
        f.write_str(")")?;
        Ok(())
    }
}

pub struct OpaqueTraitRc(u128, std::marker::PhantomData<*mut ()>);

impl OpaqueTraitRc {
    #[inline]
    pub fn wrap<T: ?Sized>(t: Rc<T>) -> OpaqueTraitRc {
        if std::mem::size_of::<*const T>() != std::mem::size_of::<u128>() {
            Self::panic_on_size_mismatch::<T>();
        }
        let t = Rc::into_raw(t);
        let value = unsafe { std::ptr::read(&t as *const _ as *const u128) };
        OpaqueTraitRc(value, Default::default())
    }

    #[inline]
    pub unsafe fn unwrap<T: ?Sized>(self) -> Rc<T> {
        let ptr = std::ptr::read(&self.0 as *const _ as *const *const T);
        Rc::from_raw(ptr)
    }

    #[cold]
    fn panic_on_size_mismatch<T: ?Sized>() {
        panic!(
            "OpaqueTraitRc::wrap(Rc<{}>) unsupported type (size = {}), expected Rc<dyn Trait>",
            std::any::type_name::<T>(),
            std::mem::size_of::<*const T>()
        );
    }
}

impl<'a> std::fmt::Debug for OpaqueTraitRc {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("OpaqueTraitRc(")?;
        write!(f, "0x{:x}, 0x{:x}", self.0 as u64, (self.0 >> 64) as u64)?;
        f.write_str(")")?;
        Ok(())
    }
}

pub struct OpaqueTraitBox(u128, std::marker::PhantomData<*mut ()>);

impl OpaqueTraitBox {
    #[inline]
    pub fn wrap<T: ?Sized>(t: Box<T>) -> OpaqueTraitBox {
        if std::mem::size_of::<*mut T>() != std::mem::size_of::<u128>() {
            Self::panic_on_size_mismatch::<T>();
        }
        let t = Box::into_raw(t);
        let value = unsafe { std::ptr::read(&t as *const _ as *const u128) };
        OpaqueTraitBox(value, Default::default())
    }

    #[inline]
    pub unsafe fn unwrap<T: ?Sized>(self) -> Box<T> {
        let ptr = std::ptr::read(&self.0 as *const _ as *const *mut T);
        Box::from_raw(ptr)
    }

    #[cold]
    fn panic_on_size_mismatch<T: ?Sized>() {
        panic!(
            "OpaqueTraitBox::wrap(Box<{}>) unsupported type (size = {}), expected Box<dyn Trait>",
            std::any::type_name::<T>(),
            std::mem::size_of::<*mut T>()
        );
    }
}

impl<'a> std::fmt::Debug for OpaqueTraitBox {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("OpaqueTraitBox(")?;
        write!(f, "0x{:x}, 0x{:x}", self.0 as u64, (self.0 >> 64) as u64)?;
        f.write_str(")")?;
        Ok(())
    }
}
