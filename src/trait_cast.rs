use std::{any::TypeId, rc::Rc, sync::Arc};

use crate::{OpaqueTrait, OpaqueTraitArc, OpaqueTraitBox, OpaqueTraitRc};

/// Structs that require trait casting functionality should extend this trait.
/// Important: Avoid direct implementation. Utilize the `implement_trait_cast!(...)` macro.
pub trait TraitCast {
    fn supported_traits(&self) -> &'static [&'static str];

    fn internal_has_trait(&self, boxed_type_id: TypeId) -> bool;

    unsafe fn internal_query_trait<'a>(&'a self, boxed_type_id: TypeId) -> Option<OpaqueTrait<'a>>;

    unsafe fn internal_query_trait_arc(
        self: Arc<Self>,
        boxed_type_id: TypeId,
    ) -> Option<OpaqueTraitArc>;

    unsafe fn internal_query_trait_rc(
        self: Rc<Self>,
        boxed_type_id: TypeId,
    ) -> Option<OpaqueTraitRc>;

    unsafe fn internal_query_trait_box(
        self: Box<Self>,
        boxed_type_id: TypeId,
    ) -> Option<OpaqueTraitBox>;
}

pub trait TraitCastExt {
    fn has_trait<T: ?Sized + 'static>(&self) -> bool;

    fn query_trait<'a, T: ?Sized + 'static>(&'a self) -> Option<&'a T>;

    fn query_trait_arc<T: ?Sized + 'static>(self: Arc<Self>) -> Option<Arc<T>>;

    fn query_trait_rc<T: ?Sized + 'static>(self: Rc<Self>) -> Option<Rc<T>>;

    fn query_trait_box<T: ?Sized + 'static>(self: Box<Self>) -> Option<Box<T>>;
}

impl<O: ?Sized + TraitCast> TraitCastExt for O {
    #[inline]
    fn has_trait<T: ?Sized + 'static>(&self) -> bool {
        self.internal_has_trait(TypeId::of::<Box<T>>())
    }

    #[inline]
    fn query_trait<'a, T: ?Sized + 'static>(&'a self) -> Option<&'a T> {
        unsafe {
            self.internal_query_trait(TypeId::of::<Box<T>>())
                .map(|t| t.unwrap())
        }
    }

    #[inline]
    fn query_trait_arc<T: ?Sized + 'static>(self: Arc<Self>) -> Option<Arc<T>> {
        unsafe {
            self.internal_query_trait_arc(TypeId::of::<Box<T>>())
                .map(|t| t.unwrap())
        }
    }

    #[inline]
    fn query_trait_rc<T: ?Sized + 'static>(self: Rc<Self>) -> Option<Rc<T>> {
        unsafe {
            self.internal_query_trait_rc(TypeId::of::<Box<T>>())
                .map(|t| t.unwrap())
        }
    }

    #[inline]
    fn query_trait_box<T: ?Sized + 'static>(self: Box<Self>) -> Option<Box<T>> {
        unsafe {
            self.internal_query_trait_box(TypeId::of::<Box<T>>())
                .map(|t| t.unwrap())
        }
    }
}

#[macro_export]
macro_rules! implement_trait_cast {
    ($type:ty, $($trait_name:ident),*) => {
        impl $crate::TraitCast for $type {
            fn supported_traits(&self) -> &'static [&'static str] {
                &[
                    $(
                        stringify!($trait_name)
                    ),*
                ]
            }

            fn internal_has_trait(&self, boxed_type_id: std::any::TypeId) -> bool {
                $(
                    if std::any::TypeId::of::<Box<dyn $trait_name>>() == boxed_type_id {
                        return true;
                    }
                )*
                false
            }

            unsafe fn internal_query_trait<'a>(
                &'a self, boxed_type_id: std::any::TypeId
            ) -> Option<$crate::OpaqueTrait<'a>> {
                $(
                    if std::any::TypeId::of::<Box<dyn $trait_name>>() == boxed_type_id {
                        return Some($crate::OpaqueTrait::wrap(self as &dyn $trait_name));
                    }
                )*
                None
            }

            unsafe fn internal_query_trait_arc(
                self: std::sync::Arc<Self>,
                boxed_type_id: std::any::TypeId,
            ) -> Option<$crate::OpaqueTraitArc> {
                $(
                    if std::any::TypeId::of::<Box<dyn $trait_name>>() == boxed_type_id {
                        return Some($crate::OpaqueTraitArc::wrap(self as std::sync::Arc<dyn $trait_name>));
                    }
                )*
                None
            }

            unsafe fn internal_query_trait_rc(
                self: std::rc::Rc<Self>,
                boxed_type_id: std::any::TypeId,
            ) -> Option<$crate::OpaqueTraitRc> {
                $(
                    if std::any::TypeId::of::<Box<dyn $trait_name>>() == boxed_type_id {
                        return Some($crate::OpaqueTraitRc::wrap(self as std::rc::Rc<dyn $trait_name>));
                    }
                )*
                None
            }

            unsafe fn internal_query_trait_box(
                self: Box<Self>,
                boxed_type_id: std::any::TypeId,
            ) -> Option<$crate::OpaqueTraitBox> {
                $(
                    if std::any::TypeId::of::<Box<dyn $trait_name>>() == boxed_type_id {
                        return Some($crate::OpaqueTraitBox::wrap(self as Box<dyn $trait_name>));
                    }
                )*
                None
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use super::{TraitCast, TraitCastExt};

    #[test]
    fn test_query_trait() {
        let obj = TestObject::new(1, "abc");
        let named = obj.query_trait_box::<dyn NamedObject>().unwrap();
        assert_eq!(named.name(), "abc");
        let readable = named.query_trait_box::<dyn ReadableObject>().unwrap();
        assert_eq!(readable.read(), "abc");

        let obj: Arc<dyn ReadableObject> = Arc::from(readable);
        let obj = obj.query_trait_arc::<dyn Object>().unwrap();
        assert!(obj.has_trait::<dyn Object>());
        assert!(obj.has_trait::<dyn NamedObject>());
        assert!(obj.has_trait::<dyn ReadableObject>());
        assert_eq!(obj.query_trait::<dyn NamedObject>().unwrap().name(), "abc");

        assert_eq!(
            obj.supported_traits(),
            &["Object", "NamedObject", "ReadableObject"]
        );
    }

    trait Object: TraitCast + 'static {
        fn id(&self) -> usize;
    }

    trait NamedObject: Object {
        fn name(&self) -> &str;
    }

    trait ReadableObject: Object {
        fn read(&self) -> &str;
    }

    struct TestObject {
        id: usize,
        name: String,
    }

    implement_trait_cast!(TestObject, Object, NamedObject, ReadableObject);

    impl TestObject {
        pub fn new(id: usize, name: &str) -> Box<dyn Object> {
            Box::new(TestObject {
                id,
                name: name.to_string(),
            })
        }
    }

    impl Object for TestObject {
        fn id(&self) -> usize {
            self.id
        }
    }

    impl NamedObject for TestObject {
        fn name(&self) -> &str {
            &self.name
        }
    }

    impl ReadableObject for TestObject {
        fn read(&self) -> &str {
            &self.name
        }
    }
}
