//! # Overview
//!
//! The `trait_cast` crate offers functionality similar to COM's "QueryInterface",
//! but for concrete types in Rust that implement a hierarchy of object-safe traits.
//! More specifically, if you have a struct `S` that implements potentially unrelated
//! traits `T1` and `T2`, `trait_cast` allows you to derive a `&dyn T2` from a
//! `&dyn T1`, provided that `S` has opted into the `trait_cast` mechanism.
//!
//! # Usage
//!
//! ```rust
//! use std::sync::Arc;
//! use trait_cast::*;
//!
//! trait Object: TraitCast {}
//!
//! trait Shape: Object {
//!     fn describe(&self) -> &str;
//! }
//!
//! trait Visual: Object {
//!     fn draw(&self) -> Result<(), ()>;
//! }
//!
//! struct Thingy {}
//!
//! impl Object for Thingy {}
//!
//! impl Shape for Thingy {
//!     fn describe(&self) -> &str { "circle" }
//! }
//!
//! impl Visual for Thingy {
//!     fn draw(&self) -> Result<(), ()> { Ok(()) }
//! }
//!
//! implement_trait_cast!(Thingy, Object, Shape, Visual);
//!
//! fn create() -> Box<dyn Object> { Box::new(Thingy {}) }
//!
//! let obj = create();
//!
//! // Checking whether a trait is implemented by an instance:
//! assert!(obj.has_trait::<dyn Shape>());
//! assert!(obj.has_trait::<dyn Visual>());
//!
//! // Turn a `Box<dyn Object> into a `Box<dyn Shape>` (downcasting boxed traits):
//! let shape = obj.query_trait_box::<dyn Shape>().expect("into shape");
//! assert_eq!(shape.describe(), "circle");
//!
//! // Cast a `&dyn Shape` to a `&dyn Visual`:
//! let visual_ref: &dyn Visual = shape.query_trait().expect("as visual");
//! assert!(visual_ref.draw().is_ok());
//!
//! let shape_arc: Arc<dyn Shape> = Arc::from(shape);
//! // Cast an `Arc<dyn Shape>` into an `Arc<dyn Visual>`:
//! let visual_arc = shape_arc.query_trait_arc::<dyn Visual>().expect("into visual");
//! assert!(visual_arc.draw().is_ok());
//!
//! let object_arc = visual_arc.query_trait::<dyn Object>().expect("upcast");
//! ```
//!

mod opaque_trait;
mod trait_cast;

pub use opaque_trait::{OpaqueTrait, OpaqueTraitArc, OpaqueTraitBox, OpaqueTraitRc};
pub use trait_cast::{TraitCast, TraitCastExt};
